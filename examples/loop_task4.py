import asyncio
import functools

async def holacoro(v):
    print("Hola %d" % v)
    await asyncio.sleep(1)
    return v+1
    
async def sumacoro(*args):
    c=sum(args)
    print("La suma es %d" %c)
    await asyncio.sleep(3)
    return c
    
def fargs_holacoro(args, obtained):
    return [obtained]
    
def fargs_sumacoro(args, obtained):
    result= [args[-1], obtained]
    return result
    
#every coroutine
async def coromask(coro, args, fargs):
     _in=args
     obtained=await coro(*args)
     result=fargs(_in, obtained)
     return result
   
def renew(task, coro, fargs, *args):
    result=task.result()
    task=loop.create_task(coromask(coro, result, fargs))
    task.add_done_callback(functools.partial(renew, task, coro, fargs))

loop=asyncio.get_event_loop()

args=[1]
task1=loop.create_task(coromask(holacoro,args,fargs_holacoro))
task1.add_done_callback(functools.partial(renew, task1, holacoro, fargs_holacoro))

args2=[1,2]
task2=loop.create_task(coromask(sumacoro,args2,fargs_sumacoro))
task2.add_done_callback(functools.partial(renew, task2, sumacoro, fargs_sumacoro))


try:
     loop.run_forever()
except KeyboardInterrupt:
     print('Loop stopped')

