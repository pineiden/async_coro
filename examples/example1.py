import asyncio

async def holacoro():
	for i in range(3):
		await asyncio.sleep(1)
		print("Hola %d" % i)

if __name__ == "__main__":
	loop = asyncio.get_event_loop()
	#creamos tarea y la asociamos al loop, ejecutandola
	loop.run_until_complete(holacoro())
